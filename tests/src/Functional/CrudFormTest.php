<?php

namespace Drupal\Tests\gsso\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Test module.
 *
 * @group gsso
 */
class CrudFormTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'gsso',
    'group',
    'node',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'claro';

  /**
   * Test access to site from anonymous users.
   */
  public function testAnonymousAccessForm() {
    // Going to the config page.
    $this->drupalGet('/admin/group/sso');

    // Checking that the page is not accesible for anonymous users.
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Test access to configuration page.
   */
  public function testCanAccessConfigPage() {
    $account = $this->drupalCreateUser([
      'administer group',
    ]);

    $this->drupalLogin($account);
    $this->drupalGet('/admin/group/sso');
    $this->assertText('Group SSO settings');
  }

  /**
   * Test submit to configuration page.
   */
  public function testSubmitConfigPage() {
    $account = $this->drupalCreateUser([
      'administer group',
    ]);

    $this->drupalLogin($account);
    $this->drupalGet('/admin/group/sso');
    $this->assertSession()->statusCodeEquals(200);

    $submit_button = 'Save configuration';
    $this->assertSession()->buttonExists($submit_button);
  }

}
