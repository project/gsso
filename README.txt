CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The Group SSO (gsso) module allows you to manage group permissions based
on a selected user attribute. The module makes use of the following
configuration example:

Config example:
  'sso_type' => 'saml',
  'sso_group_attribute' => 'xpto 1',
  'sso_role_attribute' => 'xpto 2',
  'sso_separator' => 'eol',
  'role 1' => 'claims',
  'role 2' => 'claims',
  'sso_claims' => [
    'claim 1' => [
      'roles' => [
        'role 1',
        'role 2'
      ],
      'groups' => [
        'group 1' => [
          'group role 1',
          'group role 2'
        ],
        'group 2' => [
          'group role 1'
        ],
      ],
    ],
  ],
  'table' => <table values>

Therefore it is possible to manage user access to roles, groups and group roles,
using claims from an identity manager.


REQUIREMENTS
------------

This module requires the following modules:

 * Group (https://www.drupal.org/project/group)


RECOMMENDED MODULES
-------------------

 * simpleSAMLphp Authentication
   (https://www.drupal.org/project/simplesamlphp_auth):
   When enabled, makes it possible for Drupal to communicate with SAML
   or Shibboleth identity providers (IdP) for authenticating users.

 * SAML Extras (https://www.drupal.org/project/saml_extras):
   When enabled, maps user fields with simpleSAMLphp attributes during
   user authentication.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.


CONFIGURATION
-------------

 * Configure the user permissions in Administration » Groups » SAML:

   - Select the SSO type

   - Select the Group Attribute

   - Select the Role Attribute

   - Select the separator

   - Fill the roles tables, as needed

   - Fill the group by roles matrix, as needed

     In each box of the matrix you should add the string which provide access
     to that combination of group/role.

   - Save the configuration

MAINTAINERS
-----------

Current maintainers:
 * Debora Antunes (dgaspara) - https://www.drupal.org/u/dgaspara
 * Hugo Costa Cabral (hugoccabral) - https://www.drupal.org/u/hugoccabral
 * João Marques (joaomarques736) - https://www.drupal.org/u/joaomarques736
 * Nuno Ramos (-nrzr-) - https://www.drupal.org/u/nrzr
 * Ricardo Tenreiro (ricardotenreiro) - https://www.drupal.org/u/ricardotenreiro

This project has been sponsored by:
 * everis
    Multinational consulting firm providing business and strategy solutions,
    application development, maintenance, and outsourcing services. Being part
    of the NTT Data group enables everis to offer a wider range of solutions and
    services through increased capacity as well as technological, geographical,
    and financial resources.
