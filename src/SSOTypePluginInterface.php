<?php

namespace Drupal\gsso;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * SSOTypePluginInterface class.
 */
interface SSOTypePluginInterface extends PluginInspectionInterface, ContainerFactoryPluginInterface {

}
