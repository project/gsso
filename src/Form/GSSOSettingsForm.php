<?php

namespace Drupal\gsso\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\user\RoleInterface;

/**
 * Class GSSOSettingsForm.
 */
class GSSOSettingsForm extends AbstractGSSOSettingsForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config(static::SETTINGS);
    /*
     * Config example:
     *
     * 'sso_type' => 'saml',
     * 'sso_group_attribute' => 'xpto 1',
     * 'sso_role_attribute' => 'xpto 2',
     * 'sso_separator' => 'eol',
     * 'role 1' => 'claims',
     * 'role 2' => 'claims',
     * 'sso_claims' => [
     *    'claim 1' => [
     *      'roles' => [
     *        'role 1',
     *        'role 2'
     *      ],
     *      'groups' => [
     *        'group 1' => [
     *          'group role 1',
     *          'group role 2'
     *        ],
     *        'group 2' => [
     *          'group role 1'
     *        ],
     *      ],
     *    ],
     * ],
     * 'table' => <table values>
     */
    $form['configuration'] = [
      '#type' => 'details',
      '#title' => $this->t('Configuration'),
    ];

    // SSO type.
    $type = \Drupal::service('plugin.manager.sso_types');
    $plugin_definitions = $type->getDefinitions();
    $sso_types = [];
    foreach ($plugin_definitions as $sso_type) {
      $sso_types[$sso_type['id']] = $sso_type['name'];
    }
    $form['configuration']['sso_type'] = [
      '#type' => 'radios',
      '#options' => $sso_types,
      '#default_value' => $config->get('sso_type') ?: 'saml',
      '#title' => $this->t('Subscribe to these types of SSO'),
    ];

    // SSO Group attribute.
    $form['configuration']['sso_group_attribute'] = [
      '#type' => 'textfield',
      '#title' => $this->t('SSO Group Attribute'),
      '#default_value' => $config->get('sso_group_attribute') ?: '',
    ];
    // SSO Role attribute.
    $form['configuration']['sso_role_attribute'] = [
      '#type' => 'textfield',
      '#title' => $this->t('SSO Role Attribute'),
      '#default_value' => $config->get('sso_role_attribute') ?: '',
    ];

    // SSO separator.
    $form['configuration']['sso_separator'] = [
      '#type' => 'select',
      '#options' => [
        'eol' => 'EOL (new line)',
        'comma' => ',',
        'dot' => '.',
        'pipe' => '|',
      ],
      '#default_value' => $config->get('sso_separator') ?: 'eol',
      '#title' => $this->t('Separator'),
    ];

    // Roles claims.
    $form['configuration']['configuration_roles'] = [
      '#type' => 'details',
      '#title' => $this->t('Roles'),
    ];
    $exclude_roles = [
      RoleInterface::ANONYMOUS_ID,
      RoleInterface::AUTHENTICATED_ID,
    ];
    $roles = $this->entityTypeManager->getStorage('user_role')->loadMultiple();
    ksort($roles);
    foreach ($roles as $role) {
      if (in_array($role->id(), $exclude_roles)) {
        continue;
      }
      $form['configuration']['configuration_roles'][$role->id()] = [
        '#type' => 'textarea',
        '#title' => $role->label(),
        '#default_value' => $config->get($role->id()) ?: '',
        '#cols' => 40,
        '#rows' => 2,
        '#resizable' => 'vertical',
      ];
    }

    // Group Roles claims.
    $rows = [];
    $groups = $this->entityTypeManager->getStorage('group')->loadMultiple();
    foreach ($groups as $gid => $group) {
      $rows[$gid] = $group->label();
    }
    $table_value = $config->get('table') ?: [];
    $form['sso_claims'] = $this->renderTable($rows, $table_value);

    // Apply to changes only?
    $form['configuration']['claims_changes'] = [
      '#type' => 'checkbox',
      '#default_value' => $config->get('claims_changes') ?: FALSE,
      '#title' => $this->t('Update only on claims change?'),
    ];

    // Activate debug.
    $form['configuration']['debug'] = [
      '#type' => 'checkbox',
      '#default_value' => $config->get('debug') ?: FALSE,
      '#title' => $this->t('Activate debug'),
    ];

    // GSSO library.
    $form['#attached']['library'][] = 'gsso/gsso';
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory->getEditable(static::SETTINGS);
    $table = $form_state->getValue('sso_claims');
    $sso_claims = [];
    if (($separator = $form_state->getValue('sso_separator')) === 'eol') {
      $separator = PHP_EOL;
    }
    foreach ($table as $gid => $row) {
      foreach ($row as $role => $claims) {
        if ($claims_arr = $this->getClaimsArray($claims, $separator)) {
          foreach ($claims_arr as $claim) {
            $claim = trim($claim);
            if ($claim === '') {
              continue;
            }
            if (array_key_exists($claim, $sso_claims)) {
              if (array_key_exists('groups', $sso_claims[$claim])) {
                if (array_key_exists($gid, $sso_claims[$claim]['groups'])) {
                  if (!in_array($role, $sso_claims[$claim]['groups'][$gid])) {
                    // Assign group_role to claim, if it is not assigned yet.
                    $sso_claims[$claim]['groups'][$gid][] = $role;
                  }
                }
                else {
                  // Assign group_role to claim, if it is not assigned yet.
                  $sso_claims[$claim]['groups'][$gid] = [$role];
                }
              }
              else {
                // Assign group_role to claim, if it is not assigned yet.
                $sso_claims[$claim]['groups'] = [$gid => [$role]];
              }
            }
            else {
              $sso_claims[$claim] = ['groups' => [$gid => [$role]]];
            }
          }
        }
      }
    }

    foreach ($this->entityTypeManager->getStorage('user_role')->loadMultiple() as $role) {
      $rid = $role->id();
      $claims = $form_state->getValue($rid);
      $config->set($rid, $claims);
      if ($claims_arr = $this->getClaimsArray($claims, $separator)) {
        foreach ($claims_arr as $claim) {
          $claim = trim($claim);
          if ($claim === '') {
            continue;
          }
          if (array_key_exists($claim, $sso_claims)) {
            if (array_key_exists('roles', $sso_claims[$claim])) {
              if (!in_array($rid, $sso_claims[$claim]['roles'])) {
                // Assign role to claim, if it is not assigned yet.
                $sso_claims[$claim]['roles'][] = $rid;
              }
            }
            else {
              // Assign role to claim.
              $sso_claims[$claim]['roles'] = [$rid];
            }
          }
          else {
            $sso_claims[$claim] = ['roles' => [$rid]];
          }
        }
      }
    }

    $config->set('sso_type', $form_state->getValue('sso_type'))
      ->set('claims_changes', $form_state->getValue('claims_changes'))
      ->set('debug', $form_state->getValue('debug'))
      ->set('sso_group_attribute', $form_state->getValue('sso_group_attribute'))
      ->set('sso_role_attribute', $form_state->getValue('sso_role_attribute'))
      ->set('sso_separator', $form_state->getValue('sso_separator'))
      ->set('sso_claims', $sso_claims)
      ->set('table', $table)
      ->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * Explode claims into an array, using separator.
   *
   * @param string $claims
   *   Claims string.
   * @param string $separator
   *   Separator to break claims.
   */
  private function getClaimsArray($claims, $separator) {
    // If no claims, ignore.
    if ($claims === '') {
      return FALSE;
    }
    $claims_arr = explode($separator, $claims);
    $claims_arr = array_filter($claims_arr, function ($value) {
      return !is_null($value) && $value !== '' && $value !== 0;
    });
    // If unvalid claims, ignore.
    if (empty($claims_arr)) {
      return FALSE;
    }
    return $claims_arr;
  }

}
