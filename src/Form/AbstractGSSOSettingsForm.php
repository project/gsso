<?php

namespace Drupal\gsso\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class GSSOSettingsForm.
 */
abstract class AbstractGSSOSettingsForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'gsso.settings';

  /**
   * Provides an interface for an entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Provides an interface for entity type managers.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * GSSO Form constructor.
   *
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   Provides an interface for an entity field manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Provides an interface for entity type managers.
   */
  public function __construct(EntityFieldManagerInterface $entity_field_manager, EntityTypeManagerInterface $entity_type_manager) {
    $this->entityFieldManager = $entity_field_manager;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_field.manager'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'gsso_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * Render table markup.
   */
  public function renderTable($rows, $table_values) {
    $table = [
      '#type' => 'table',
      '#attributes' => [
        'id' => ['edit-output'],
      ],
    ];
    $group_types_roles = [];
    $this->renderHeader($table, $group_types_roles);
    $this->renderRows($table, $rows, $group_types_roles, $table_values);
    return $table;
  }

  /**
   * Render table header markup.
   */
  public function renderHeader(&$table, &$group_types_roles) {
    $header['groups'] = $this->t('Groups');
    foreach ($this->entityTypeManager->getStorage('group_type')->loadMultiple() as $gtid => $group_type) {
      $group_types_roles[$gtid] = [];
      /** @var \Drupal\group\Entity\GroupType $group_type */
      $header[$group_type->getMemberRoleid()] = $group_type->label() . ':' . $this->t('@group_role', ['@group_role' => $group_type->getMemberRole()->label()]);
      $group_types_roles[$gtid][] = $group_type->getMemberRoleId();
      foreach ($group_type->getRoles(FALSE) as $group_role) {
        $header[$group_role->id()] = $group_role->getGroupType()->label() . ':' . $this->t('@group_role', ['@group_role' => $group_role->label()]);
        $group_types_roles[$gtid][] = $group_role->id();
      }
    }

    $table['#header'] = $header;
  }

  /**
   * Render table rows markup.
   */
  public function renderRows(&$table, $rows, $group_types_roles, $table_values) {
    $header = $table['#header'];
    $header_keys = array_keys($header);
    $rows_keys = array_keys($rows);
    for ($j = 0; $j < count($rows_keys); $j++) {
      $group_name = $rows[$rows_keys[$j]];
      $group = $this->entityTypeManager->getStorage('group')->load($rows_keys[$j]);
      /** @var \Drupal\group\Entity\Group $group */
      $group_type = $group->getGroupType()->id();
      for ($i = 0; $i < count($header); $i++) {
        $hk = $header_keys[$i];
        $rk = $rows_keys[$j];
        if ($hk == 'groups') {
          $table[$rk][$hk] = [
            '#type' => 'label',
            '#title' => $group_name,
            '#title_display' => 'before',
          ];
        }
        else {
          if (in_array($hk, $group_types_roles[$group_type])) {
            $table[$rk][$hk] = [
              '#type' => 'textarea',
              '#title' => 'Text',
              '#title_display' => 'hidden',
              '#default_value' => isset($table_values[$rk], $table_values[$rk][$hk]) ? $table_values[$rk][$hk] : '',
              '#cols' => 40,
              '#rows' => 1,
              '#resizable' => 'vertical',
            ];
          }
          else {
            $table[$rk][$hk] = [
              '#type' => 'markup',
              '#markup' => '<div class="text-center">-</div>',
            ];
          }
        }
      }
    }
  }

}
