<?php

namespace Drupal\gsso\Plugin\SSOType;

use Drupal\gsso\SSOTypePluginBase;

/**
 * SAML SSO Type.
 *
 * @SSOType(
 *   id = "saml",
 *   name = @Translation("SAML")
 * )
 */
class SAML extends SSOTypePluginBase {

}
