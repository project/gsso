<?php

namespace Drupal\gsso;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Provides an SSOType plugin manager.
 *
 * @see plugin_api
 */
class SSOTypeManager extends DefaultPluginManager {

  /**
   * {@inheritdoc}
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/SSOType',
      $namespaces,
      $module_handler,
      'Drupal\gsso\SSOTypePluginInterface',
      'Drupal\gsso\Annotation\SSOType');
    $this->alterInfo('gsso_info');
    $this->setCacheBackend($cache_backend, 'gsso');
  }

}
