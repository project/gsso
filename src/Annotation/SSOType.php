<?php

namespace Drupal\gsso\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a SSO Type plugin annotation object.
 *
 * @Annotation
 */
class SSOType extends Plugin {

}
