<?php

namespace Drupal\gsso\Services;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Exception;
use Drupal\group\GroupMembershipLoaderInterface;
use Drupal\user\UserInterface;

/**
 * GSSO class.
 */
class GSSO {

  /**
   * GSSO Claims table.
   *
   * @var string
   */
  const GSSO_CLAIMS = 'gsso_claims';

  /**
   * GSSO Claims 'user id' field.
   *
   * @var string
   */
  const GSSO_CLAIMS_UID = 'uid';

  /**
   * GSSO Claims 'claims' field.
   *
   * @var string
   */
  const GSSO_CLAIMS_CLAIMS = 'claims';

  /**
   * GSSO Claims logger class.
   *
   * @var string
   */
  const GSSO_LOGGER = 'gsso';

  /**
   * Provides an interface for an entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Provides an interface for entity type managers.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Provides a database connection service.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * GSSO constructor.
   *
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   Provides an interface for an entity field manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Provides an interface for entity type managers.
   * @param \Drupal\Core\Database\Connection $connection
   *   Provides a database connection service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   The logger service the instance should use.
   * @param \Drupal\group\GroupMembershipLoaderInterface $group_membership_loader
   *   The group membership loader.
   */
  public function __construct(EntityFieldManagerInterface $entity_field_manager, EntityTypeManagerInterface $entity_type_manager, Connection $connection, LoggerChannelFactoryInterface $logger, GroupMembershipLoaderInterface $group_membership_loader) {
    $this->entityFieldManager = $entity_field_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->connection = $connection;
    $this->logger = $logger;
    $this->groupMembershipLoader = $group_membership_loader;
  }

  /**
   * Get user current claims.
   *
   * @param int $uid
   *   User ID.
   */
  public function getUserClaims(int $uid) {
    return $this->connection->select(static::GSSO_CLAIMS)
      ->fields(static::GSSO_CLAIMS, [static::GSSO_CLAIMS_CLAIMS])
      ->condition(static::GSSO_CLAIMS_UID, $uid)
      ->range(0, 1)
      ->execute()
      ->fetchField();
  }

  /**
   * Update user claims.
   *
   * @param int $uid
   *   User ID.
   * @param string $claims
   *   New user claims.
   * @param bool $debug
   *   Show debug values.
   */
  public function setUserClaims(int $uid, string $claims, $debug = FALSE) {
    try {
      $this->connection->merge(static::GSSO_CLAIMS)
        ->key(static::GSSO_CLAIMS_UID, $uid)
        ->fields([
          static::GSSO_CLAIMS_CLAIMS => $claims,
        ])
        ->execute();
      if ($debug) {
        $this->logger->get(static::GSSO_LOGGER)->debug('Updated @table for @uid with @claims', [
          '@table' => static::GSSO_CLAIMS,
          '@uid' => $uid,
          '@claims' => $claims,
        ]);
      }
    }
    catch (Exception $e) {
      $this->logger->get(static::GSSO_LOGGER)->warning('Error when trying to update @table for @uid with @claims', [
        '@table' => static::GSSO_CLAIMS,
        '@uid' => $uid,
        '@claims' => $claims,
      ]);
    }
  }

  /**
   * Get user groups.
   *
   * @param \Drupal\user\UserInterface $account
   *   Account to search for.
   */
  public function getUserGroups(UserInterface $account = NULL) {
    $groups = [];
    $grps = $this->groupMembershipLoader->loadByUser($account);
    foreach ($grps as $grp) {
      $groups[] = $grp->getGroup();
    }
    return $groups;
  }

  /**
   * Remove user from all his groups.
   *
   * @param \Drupal\user\UserInterface $account
   *   Account to remove from groups.
   * @param bool $debug
   *   Show debug values.
   */
  public function removeUserMemberships(UserInterface $account = NULL, $debug = FALSE) {
    $roles = $account->getRoles(TRUE);
    foreach ($roles as $role) {
      $account->removeRole($role);
      if ($debug) {
        $this->logger->get(static::GSSO_LOGGER)->debug('User @account successfully removed from role @role', [
          '@account' => $account->id(),
          '@role' => $role,
        ]);
      }
    }
    $account->save();

    $groups = $this->getUserGroups($account);
    if (!empty($groups)) {
      foreach ($groups as $group) {
        $transaction = $this->connection->startTransaction();
        try {
          $group->removeMember($account);
          if ($debug) {
            $this->logger->get(static::GSSO_LOGGER)->debug('User @account successfully removed from group @group', [
              '@account' => $account->id(),
              '@group' => $group->id(),
            ]);
          }
        }
        catch (Exception $e) {
          $transaction->rollBack();
          $this->logger->get(static::GSSO_LOGGER)->error('Could not remove user @account from group @group', [
            '@account' => $account->id(),
            '@group' => $group->id(),
          ]);
        }

        unset($transaction);
      }
    }
  }

  /**
   * Associate a User to his respective groups.
   *
   * @param \Drupal\user\UserInterface $user
   *   User to associate to.
   * @param array $fss
   *   Claims.
   * @param array $mappings
   *   Mapping from the claims to roles.
   * @param bool $debug
   *   Show debug values.
   */
  public function associateUserToGroups(UserInterface $user, array $fss = [], array $mappings = [], $debug = FALSE) {
    if (empty($fss) || empty($mappings)) {
      return;
    }

    foreach ($fss as $fs) {
      if (array_key_exists($fs, $mappings)) {
        $mapping = $mappings[$fs];
        if (array_key_exists('roles', $mapping) && !empty($roles = $mapping['roles'])) {
          foreach ($roles as $rid) {
            $user->addRole($rid);
            if ($debug) {
              $this->logger->get(static::GSSO_LOGGER)->debug('User @account successfully added to role @role', [
                '@account' => $user->id(),
                '@role' => $rid,
              ]);
            }
          }
          $user->save();
        }
        if (array_key_exists('groups', $mapping) && !empty($groups = $mapping['groups'])) {
          foreach ($groups as $gid => $roles) {
            foreach ($roles as $k => $role) {
              if (strpos($role, '-member') > 0) {
                unset($roles[$k]);
                break;
              }
            }
            $group = $this->entityTypeManager->getStorage('group')->load($gid);
            /** @var \Drupal\group\Entity\Group $group */
            $group->addMember($user, ['group_roles' => $roles]);
            if ($debug) {
              $this->logger->get(static::GSSO_LOGGER)->debug('User @account successfully added to group @group', [
                '@account' => $user->id(),
                '@group' => $group->id(),
              ]);
            }
          }
        }
      }
    }
  }

}
